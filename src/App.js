import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../src/style/singup.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

import Login from "./components/Login"
// import SignUp from './components/SignUp';
// import ForgetPassword from './components/ForgetPassword';
import Dashboard from './components/Dashboard/Dashboard'
import DashBody from './components/Dashboard/DashBody'
import SubAdminList from './components/Dashpages/SubAdminList'
import SubAdminAdd from './components/Dashpages/SubAdminAdd'
import UsersAdd from './components/Dashpages/UserAdd'
import UsersList from './components/Dashpages/UsersList'
import TransactionHistory from "./components/Dashpages/TransactionHistory"
import WithdrawRequest from './components/Dashpages/WithdrawRequest'
import Settings from './components/Dashpages/Settings'
import ChangePassword from './components/Dashpages/ChangePassword'
import Profile from './components/Dashpages/Profile'
import UserKyc from './components/Dashpages/UserKyc'
import BankDetails from './components/Dashpages/BankDetails'
import HelpDesk from './components/Dashpages/HelpDesk'
import { useHistory} from "react-router-dom"

function App() {
  //check user logged in or not
  const history = useHistory();
  if (localStorage.getItem("user-info") && !localStorage.getItem("user-info").user_id) {
    history.push("/")
  }else{
    history.push("/login")
  } 
  return (
    <Router>
      <Switch>
        <Route path="/login" exact component={Login} />
        <Switch>
          <div className="App">
            <Dashboard />,
            <Route exact path="/" component={DashBody} />
            <Route exact path="/sublist" component={SubAdminList} />
            <Route path="/subadd" component={SubAdminAdd} />
            <Route path="/useradd" component={UsersAdd} />
            <Route path="/userlist" component={UsersList} />
            <Route path="/transactionhistory" component={TransactionHistory} />
            <Route path="/withdraw" component={WithdrawRequest} />
            <Route path="/bankdetails" component={BankDetails} />
            <Route path="/settings" component={Settings} />
            <Route path="/profile" component={Profile} />
            <Route path="/password" component={ChangePassword} />
            <Route path="/userkyc" component={UserKyc} />
            <Route path="/helpdesk" component={HelpDesk} />
            {/* <Route path="/signup" component={SignUp} /> */}
            {/* <Route path="/forget" component={ForgetPassword} /> */}
            <Route path="*" component={() => "404 not found"} />
          </div>
        </Switch>
      </Switch>
    </Router>
  );
}

//guard

export default App;
