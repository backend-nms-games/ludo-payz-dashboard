import React from 'react'

const MessagesControl = (props) => {
    const {
        sendMessage,
        value,
        onChange,
        groupMessage,
        sortNames,
        username,
        reciever
    } = props;

    const messages = groupMessage ? groupMessage[sortNames(username, reciever)] : []



    return (
        <div>
            <div className="online-users-header">
                <div style={{ margin: "0 10px" }}>{reciever}</div>
            </div>
            <div className="message-area">
                <ul>
                    {messages && messages.lenght > 0
                        ? messages.map((msg, index) => (
                            <li key={index}>
                                <div className="user-pic">
                                    <img src={require(`../Helpdesk/users/${msg.avatar}`).default} alt={''} />
                                </div>
                                <div className="message-text">
                                    {msg.message}
                                </div>
                            </li>
                        )) : null};
                </ul>
            </div>
            <form onSubmit={sendMessage} className="message-control">
                <textarea
                    value={value}
                    onChange={onChange}
                    placeholder="Type something...!" />
                <div className="file-input-container">
                    <input type="file" id="hidden-file" />
                    <label htmlFor="hidden-file"><i class="fas fa-paperclip  fa-2x" ></i></label>
                </div>
                <button>
                    {/* <img src={}/> */}
                    <i class="fas fa-paper-plane"></i>
                    <span style={{ display: "inline-block" }}>Send</span>
                </button>
            </form>
        </div>
    )
}
export default MessagesControl