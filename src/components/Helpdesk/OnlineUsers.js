
const OnlineUsers = (props) => {

    const { onUserSelect, users, username } = props;
    return (
        <div>
            <div className="online-users-header">
                <div style={{ margin: "0 10px" }}>Online Users</div>
            </div>
            <ul className="users-list">
                {users && Object.keys(users).map((user, index) => (
                    <>
                        {user !== username ? (
                            <li key={{ user }} onClick={() => onUserSelect(user)}>
                                <span style={{ textTransform: "Capitalize" }}>{user}</span>
                                {/* <span className="new-message-count">3</span> */}
                            </li>
                        ) : null}
                    </>
                ))}

            </ul>
        </div>
    )
}
export default OnlineUsers