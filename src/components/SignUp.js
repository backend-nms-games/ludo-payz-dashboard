import React from "react";
import { Button, Grid, Paper, TextField, Typography, } from '@material-ui/core'

import { Link } from "react-router-dom";
// import logo from '../image/DvsT.jpg';
const SignUp = () => {

    return (
        <div >

            <Grid className="">
                <Paper elevation={15} className="paperStylesignup"  >
                    <Grid align='center'>
                        <h4>Register your account</h4>
                    </Grid>
                    <div>
                        <TextField className="my-2 d-flex justify-content-center" id="standard-basic" label="Enter your name" type='text' variant="standard" />
                    </div>

                    <div>
                        <TextField className="my-2 d-flex justify-content-center" id="standard-basic" label="Enter your email" type='email' variant="standard" />
                    </div>
                    <div >
                        <TextField className="my-2 d-flex justify-content-centered" id="standard-basic" label="Enter your password" type='password' variant="standard" />
                    </div>
                    <div >
                        <TextField className="my-2 d-flex justify-content-centered" id="standard-basic" label=" Re-enter your password" type='password' variant="standard" />
                    </div>

                    <Button className="my-4 bg-success" variant="contained" fullWidth>sign Up</Button>

                    <Typography className="my-1">
                        <p>Already have an account?
                            <Link className="text-decoration-none" to="/login">
                                Login here
                            </Link>
                        </p>

                    </Typography>
                </Paper>
            </Grid>
        </div>
    )
}
export default SignUp