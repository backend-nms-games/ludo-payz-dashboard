import React from 'react'
import Button from '@mui/material/Button';
import { Link } from 'react-router-dom'

const UsersList = () => {

    return (
        <div>
            <div className="content-wrapper">
                <section className="content-header">
                    <div className="container-fluid">
                        <div className="row mb-2">
                            <div className="col-sm-6">
                                <h1>Sub Admins List</h1>
                            </div>
                            <div className="col-sm-6">
                                <ol className="breadcrumb float-sm-right">
                                    <li className="breadcrumb-item">
                                        <Link to="/">Home</Link>
                                    </li>
                                    <li className="breadcrumb-item active">Sub Admins List</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="content sub-admins_outer">
                    <div className="r o w">
                        <div className="col-md-12">
                            <div className="card">
                                <div className="card-body">
                                    <form
                                        method="get"
                                        acceptCharset="utf-8"
                                        noValidate="novalidate"
                                        className="form-inline search_form"
                                        action="/admin/sub-admins">
                                        <div className="row">
                                            <div className="form-group col-sm-6 col-md-3">
                                                <div className="input text">
                                                    <input type="text"
                                                        name="name"
                                                        className="form-control"
                                                        placeholder="Enter Name"
                                                        id="name" />
                                                </div>
                                            </div>
                                            <div
                                                className="form-group col-sm-6 col-md-3">
                                                <div className="input email">
                                                    <input
                                                        type="email"
                                                        name="email"
                                                        className="form-control"
                                                        placeholder="Email Address"
                                                        id="email" />
                                                </div>
                                            </div>
                                            <div className="form-group col-sm-6 col-md-3">
                                                <div className="input tel">
                                                    <input
                                                        type="tel"
                                                        name="phone"
                                                        className="form-control"
                                                        placeholder="Phone Number"
                                                        id="phone" />
                                                </div>
                                            </div>
                                            <div className="form-group col-sm-6 col-md-3">
                                                <div className="input text">
                                                    <input type="text"
                                                        name="start_date"
                                                        readOnly="readonly"
                                                        className="form-control datepicker-input start_date"
                                                        placeholder="Enter Registered From"
                                                        id="start-date" />
                                                </div>
                                            </div>

                                            <div className="form-group col-sm-6 col-md-3 my-3
                                                ">
                                                <div className="input text">
                                                    <input type="text"
                                                        name="end_date"
                                                        readOnly="readonly"
                                                        className="form-control datepicker-input end_date"
                                                        placeholder="To"
                                                        id="end-date" />

                                                </div>
                                            </div>
                                            <div className="form-group col-sm-6 col-md-3 ">

                                                <Button
                                                    type="submit"
                                                    variant="outlined"
                                                    className="btn btn-default site_btn_color mx-1 ">
                                                    Search
                                                </Button>
                                                <Button variant="outlined" href="/sublist">
                                                    <i className="fa fa-undo" />
                                                    Reset
                                                </Button>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                                <div className="card-header">
                                    <h3 className="card-title">Sub admin Details</h3>
                                </div>
                                <div className="card-body">
                                    <div className="table-responsive">
                                        <table className="table table-bordered responsive ">
                                            <thead>
                                                <tr>

                                                    <th>#</th>
                                                    <th>
                                                        <a href="/admin/sub-admins?direction=asc&sort=Users.first_name">
                                                            Name
                                                        </a>
                                                    </th>
                                                    <th>
                                                        <a href="/admin/sub-admins?direction=asc&sort=Users.email">
                                                            Email
                                                        </a>
                                                    </th>


                                                    <th>
                                                        <a href="/admin/sub-admins?direction=asc&sort=Users.phone">
                                                            Phone Number
                                                        </a>
                                                    </th>

                                                    <th>
                                                        <a href="/admin/sub-admins?direction=asc&sort=Users.created">
                                                            Registration Date
                                                        </a>
                                                    </th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td colSpan={9} className="text-center">No Record Found</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div >

    )

}
export default UsersList