import React, { useState, useEffect } from 'react' 
import DataTable from 'react-data-table-component';
import { apiBaseURL } from '../../config';
import axios from "axios"
import moment from 'moment';
const UserKyc = () => {

    const [kycData, setUsers] = useState([]);
    
    const getUsers = async () => {
        await axios.get(`${apiBaseURL}/api/kyc/UsersKyc`).then(function (response) { 
            if(response.data.status ===200){
                console.log(response.data.data)
                setUsers(response.data.data)
            } 
          })
          .catch(function (error) {
            // handle error 
            console.log(error);
          }); 
    }
    useEffect(() => {
        getUsers();
        console.log(kycData)
    }, []);
    const approveStatus = async (id) => {
      // e.preventDefault();

      //  
      alert(id)
  };
    const columns = [
        {
          name: "#ID",
          selector: (row, index) => index+1,
          sortable: true
        },
        {
            name: "Phone Number",
            selector: "phone",
            string:true,
            sortable: true
          },
        {
            name: "Card Holder Name",
            selector: "card_per_name",
            string:true,
            sortable: true
          },
          {
            name: "DOB",
            cell: row => <div>{moment(row.card_dob).format("MM-DD-YYYY")}</div>,
            string:true,
            sortable: true
          },
          {
            name: "Type",
            selector: (row) => <p>{(row.is_type=='DL')?'Derving Card':(row.is_type=='VoterID')?'Voter Card':'Pan Card'}</p>
             ,
            string:true,
            sortable: true
          },
          
          {
            name: "Status",
            selector: (row) => <p>{(row.is_status==1)?'Verified':(row.is_status==2)?'Rejected':'Pending'}</p>
             ,
             
          },
          {
            name: "Actions",
            // selector: "amount",
            sortable: true, 
              },
        {
          // name: "Actions",
          cell: ( row ) => (
            <button
              aria-label="delete"
              color="secondary"
              disabled = {row.is_status==2?'Disabled':''}
              onClick={() => approveStatus(row.transaction_id)}
            >  
              Approve 
            </button>
          ),
        },{
          cell: ( row ) => (
            <button
              aria-label="delete"
              color="secondary"

              onClick={() => approveStatus(row.transaction_id)}
            >  
              Reject 
            </button>
          )
        }
          ];
          
    return (
        <div>
            <div className="content-wrapper">
                <section className="content admin_users">
                    <div className="r o w">
                        <div className="col-md-12">
                            <div className="card">
                                <div className="card-header">
                                    <h3 className="card-title" style={{ display: 'inline-block' }}></h3>
                                </div>
                                <div className="card-body">
                                  
                                <DataTable
                                    title="KYC Details"
                                    columns={columns}
                                    data={kycData}
                                    highlightOnHover
                                    pagination
                                    paginationPerPage={6}
                                    paginationRowsPerPageOptions={[6, 15, 25, 50]}
                                    paginationComponentOptions={{
                                    rowsPerPageText: 'Records per page:',
                                    rangeSeparatorText: 'out of',
                                    }}
                                />
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div >

        </div >
    )
}
export default UserKyc
