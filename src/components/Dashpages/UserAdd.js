import Axios from 'axios';
import React, { useState } from 'react'

const UserAdd = () => {
    const url = "http://65.0.213.3/pokerrz/WebServices/profile"

    const [userAddForm, setUserAddForm] = useState({
        firstname: "",
        lastname: "",
        email: "",
        phoneNumber: "",
        password: "",
        address: "",
        gender: "",
        confirm_password: ""

    });

    // const [records, setRecords] = useState([]);

    const handleSubmit = async (e) => {
        console.log("UserAdd", userAddForm)
        e.preventDefault();

        const userForm = {
            firstname: userAddForm.firstname,
            lastname: userAddForm.lastname,
            email: userAddForm.email,
            phoneNumber: userAddForm.phoneNumber,
            password: userAddForm.password,
            address: userAddForm.address,
            gender: userAddForm.gender,
            confirm_password: userAddForm.confirm_password,
        };
        Axios.post(url, userForm)
    }
    const handleInput = name => e => {
        setUserAddForm({ ...userAddForm, [name]: e.target.value });
    };

    return (
        <div>
            <div className="content-wrapper">
                <section className="content-header">
                    <div className="container-fluid">
                        <div className="row mb-2">
                            <div className="col-sm-6">
                                <h1>Add User</h1>
                            </div>
                            <div className="col-sm-6">
                                <ol className="breadcrumb float-sm-right">
                                    <li className="breadcrumb-item"><a href="/">Home</a></li>
                                    <li className="breadcrumb-item active">Add User</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="content admins_add">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="card card-primary">
                                    <div className="card-header">
                                        <h3 className="card-title">Fill Form</h3>
                                    </div>
                                    <form
                                        onSubmit={handleSubmit}>
                                        <div style={{ display: 'none' }}>
                                            <input
                                                type="hidden"
                                                name="_method"
                                                defaultValue="POST" />
                                        </div>
                                        <div className="row">
                                            <div className="col-md-12">
                                                <div className="card-body">
                                                    <div className="row">
                                                        <div className="col-sm-6 col-md-6">
                                                            <div className="form-group">
                                                                <label htmlFor="first-name">
                                                                    First Name
                                                                    <span className="text-danger">*</span>
                                                                </label>
                                                                <input
                                                                    type="text"
                                                                    name="firstname"
                                                                    value={userAddForm.firstname}
                                                                    onChange={handleInput("firstname")}
                                                                    maxLength={30}
                                                                    className="form-control"

                                                                />
                                                            </div>
                                                        </div>
                                                        <div className="col-sm-6 col-md-6">
                                                            <div
                                                                className="form-group">
                                                                <label htmlFor="last-name">
                                                                    Last Name
                                                                    <span className="text-danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input

                                                                    type="text"
                                                                    name="lastname"
                                                                    value={userAddForm.lastname}
                                                                    onChange={handleInput('lastname')}
                                                                    maxLength={30}
                                                                    className="form-control"

                                                                />
                                                            </div>
                                                        </div>
                                                        <div className="col-sm-6 col-md-6">
                                                            <div
                                                                className="form-group">
                                                                <label htmlFor="email">
                                                                    Email
                                                                    <span
                                                                        className="required">
                                                                        *
                                                                    </span>
                                                                </label>

                                                                <input
                                                                    type="email"
                                                                    name="email"
                                                                    value={userAddForm.email}
                                                                    onChange={handleInput('email')}
                                                                    className="form-control"
                                                                    placeholder="E-Mail Address"
                                                                    maxLength={255} id="email" />
                                                            </div>
                                                        </div>

                                                        <div className="col-sm-6 col-md-6">
                                                            <div className="form-group">
                                                                <label htmlFor="phone">Phone Number <span className="required">
                                                                    *
                                                                </span>
                                                                </label>
                                                                <input
                                                                    type="tel"
                                                                    name="phoneNumber"
                                                                    value={userAddForm.phoneNumber}
                                                                    onChange={handleInput('phoneNumber')}
                                                                    className="form-control"

                                                                    maxLength={20}
                                                                    id="phoneNumber" />
                                                            </div>
                                                        </div>
                                                        <div className="col-sm-6 col-md-6">
                                                            <div
                                                                className="form-group">
                                                                <label htmlFor="password">Password <span className="required">
                                                                    *
                                                                </span>
                                                                </label>
                                                                <input
                                                                    type="password"
                                                                    name="password"
                                                                    value={userAddForm.password}
                                                                    onChange={handleInput('password')}
                                                                    className="form-control"
                                                                    placeholder="Password"
                                                                    id="password" />
                                                            </div>
                                                        </div>
                                                        <div
                                                            className="col-sm-6 col-md-6">
                                                            <div
                                                                className="form-group">
                                                                <label htmlFor="gender">
                                                                    Gender
                                                                </label>
                                                                <select
                                                                    name="gender"
                                                                    value={userAddForm.gender}
                                                                    onChange={handleInput('gender')}
                                                                    className="form-control"
                                                                    id="gender">
                                                                    <option value>Select Gender</option>
                                                                    <option selected="selected">
                                                                        Male
                                                                    </option>
                                                                    <option >Female</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div className="col-sm-6 col-md-6">
                                                            <div className="form-group">
                                                                <label htmlFor="confirm-password">
                                                                    Confirm Password
                                                                    <span className="required">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input
                                                                    type="password"
                                                                    name="confirm_password"
                                                                    value={userAddForm.confirm_password}
                                                                    onChange={handleInput('confirm_password')}
                                                                    className="form-control"
                                                                    placeholder="Confirm Password"
                                                                    id="confirm-password" />
                                                            </div>
                                                        </div>
                                                        <div className="col-sm-6 col-md-6">
                                                            <div
                                                                className="form-group"><label htmlFor="address">Address</label>
                                                                <input
                                                                    type="text"
                                                                    name="address"
                                                                    value={userAddForm.address}
                                                                    onChange={handleInput('address')}
                                                                    className="form-control"
                                                                    placeholder="Address"
                                                                    id="address" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="card-footer">
                                            <button
                                                type="submit"
                                                className="btn btn-primary submit">
                                                Submit
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <div className='my-3'>hidden</div>
            </div>

        </div>
    )

}
export default UserAdd