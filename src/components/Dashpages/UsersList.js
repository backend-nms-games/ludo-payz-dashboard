import React, { useState, useEffect } from 'react'
 
import axios from 'axios';
import DataTable from 'react-data-table-component';
import { apiBaseURL } from '../../config';
const columns = [
    {
      name: "##",
      selector: (row, index) => index+1,
      sortable: true
    },
    {
        name: "Phone Number",
        selector: "phone",
        string:true,
        sortable: true
      },
    {
        name: "Username",
        selector: "username",
        string:true,
        sortable: true
      },
      {
        name: "Email ID",
        string:true,
        selector: "email",
        sortable: true
      },
      {
        name: "Device ID",
        string:true,
        selector: "device_id",
        sortable: true
      },
    {
      name: "Actions",
      selector: "amount",
      sortable: true,

      // conditionally render amount if positive or negative
      conditionalCellStyles: [
        {
          when: row => row.amount > 0,
          style: {
            color: "green"
          }
        },
        {
          when: row => row.amount < 0,
          style: {
            color: "red"
          }
        }
      ]
        },
        { 
          cell: ({ users }) => (
            <button
              aria-label="delete"
              color="secondary"
            //   onClick={() => deleteTransaction(transactions._id)}
            >  
              Delete
            </button>
          )
        },
        { 
          button:true,
            cell:row => (<div>{row.user_id}</div>),
          }
      ];
 
const UsersList = () => {

    const [users, setUsers] = useState([]);

    const getUsers = async () => {
      await axios.get(`${apiBaseURL}/api/users`).then(function (response) { 
        if(response.data.status ===200){
            console.log(response.data.data)
            setUsers(response.data.data)
        } 
      })
      .catch(function (error) {
        // handle error 
        console.log(error);
      }); 
    }
    useEffect(() => {
        getUsers();
        
    }, []);
    

    return (
        <div>
            <div className="content-wrapper">
                <section className="content admin_users">
                     
                            <div className="card"> 
                                <div className="card-body"> 
                                <DataTable
                                    title="Users Details"
                                    columns={columns}
                                    data={users}
                                    highlightOnHover
                                    pagination
                                    paginationPerPage={6}
                                    paginationRowsPerPageOptions={[6, 15, 25, 50]}
                                    paginationComponentOptions={{
                                    rowsPerPageText: 'Records per page:',
                                    rangeSeparatorText: 'out of',
                                    }}
                                />
                                 
                                    <div> 
                                    </div>
                                </div>
                            </div>
                      
                </section>
            </div> 
        </div>
    )
}
export default UsersList
