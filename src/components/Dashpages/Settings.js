import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import Axios from 'axios'

const Settings = () => {
    const url = "http://65.0.213.3/pokerrz/WebServices/profile"
    const [settingForm, setSettingForm] = useState({
        adminEmail: "",
        bonusPercentage: "",
        registrationCoin: "",
        signUpBonus: "",
        referralBouns: "",
        contestCommission: "",
        minimumWithdraw: "",
        joinLimit: "",
        version: "",
        tds: ""
    })

    const handleSubmit = async (e) => {
        console.log(settingForm)
        e.preventDefault();
        const user = {
            adminEmail: settingForm.adminEmail,
            bonusPercentage: settingForm.bonusPercentage,
            registrationCoin: settingForm.referralBouns,
            signUpBonus: settingForm.signUpBonus,
            referralBouns: settingForm.referralBouns,
            contestCommission: settingForm.contestCommission,
            minimumWithdraw: settingForm.minimumWithdraw,
            joinLimit: settingForm.joinLimit,
            version: settingForm.version,
            tds: settingForm.tds
        };
        Axios.post(url, user)
    }
    const handleInput = name => e => {
        setSettingForm({ ...settingForm, [name]: e.target.value });
    };


    return (
        <div>
            <div className="content-wrapper">
                <section className="content-header">
                    <div className="container-fluid">
                        <div className="row mb-2">
                            <div className="col-sm-6">
                                <h1>Settings</h1>
                            </div>
                            <div className="col-sm-6">
                                <ol className="breadcrumb float-sm-right">
                                    <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                                    <li className="breadcrumb-item active">Settings</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="content">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="card card-primary">
                                    <form
                                        onSubmit={handleSubmit}
                                        action="/admin/settings">
                                        <div style={{ display: 'none' }}>
                                            <input
                                                type="hidden"
                                                name="_method"
                                                defaultValue="PUT" />
                                        </div>
                                        <div className="row">
                                            <div className="col-md-12">
                                                <div className="card-body">
                                                    <div className="form-group">
                                                        <div className="input text required">
                                                            <label htmlFor="admin-email">
                                                                Admin Email
                                                                <span className="required">
                                                                    *
                                                                </span>
                                                            </label>
                                                            <input
                                                                type="text"
                                                                name="adminEmail"
                                                                value={settingForm.adminEmail}
                                                                onChange={handleInput('adminEmail')}
                                                                className="form-control"
                                                                placeholder="Admin Email"
                                                                required="required" maxLength={150}
                                                                id="admin-email"
                                                                defaultValue="admin@gmail.com" />
                                                        </div>
                                                    </div>
                                                    <div className="form-group">
                                                        <div className="input text">
                                                            <label htmlFor="admin-percentage">
                                                                Usable Bonus Percentage
                                                            </label>
                                                            <input
                                                                type="text"
                                                                name="bonusPercentage"
                                                                value={settingForm.bonusPercentage}
                                                                onChange={handleInput('bonusPercentage')}
                                                                className="form-control" placeholder="Usable Bonus Percentage"
                                                                //  onInput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');"
                                                                maxLength={50} id="admin-percentage" defaultValue={15} />
                                                        </div>
                                                    </div>
                                                    <div className="form-group">
                                                        <div className="input text">
                                                            <label htmlFor="registration-coin">User Registration Coin</label>
                                                            <input
                                                                type="text"
                                                                name="registrationCoin"
                                                                value={settingForm.registrationCoin}
                                                                onChange={handleInput('registrationCoin')}
                                                                className="form-control" placeholder="User Registration Coin"
                                                                // onInput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');"
                                                                maxLength={255}
                                                                id="registration-coin" defaultValue={10} />
                                                        </div>
                                                    </div>
                                                    <div className="form-group">
                                                        <div className="input text">
                                                            <label htmlFor="referral-bouns-amount">Signup Bouns Amount (INR)</label>
                                                            <input
                                                                type="text"
                                                                name="signUpBonus"
                                                                value={settingForm.signUpBonus}
                                                                onChange={handleInput('signUpBonus')}
                                                                maxLength={8}
                                                                className="form-control"
                                                                placeholder="Signup Bouns Amount"
                                                                id="referral-bouns-amount"
                                                                defaultValue={50} />
                                                        </div>
                                                    </div>
                                                    <div className="form-group">
                                                        <div className="input text">
                                                            <label htmlFor="referral-bouns-amount-referral">Referral Bouns Amount (INR)</label>
                                                            <input
                                                                type="text"
                                                                name="referralBouns"
                                                                value={settingForm.referralBouns}
                                                                onChange={handleInput('referralBouns')}
                                                                maxLength={8}
                                                                className="form-control"
                                                                placeholder="Referral Bouns Amount"
                                                                // onInput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');"
                                                                id="referralBouns"
                                                                defaultValue={50} />
                                                        </div>
                                                    </div>
                                                    <div className="form-group">
                                                        <div className="input text">
                                                            <label htmlFor="contest-commission">Contest Commission (In percentage)</label>
                                                            <input
                                                                type="text"
                                                                name="contestCommission"
                                                                value={settingForm.contestCommission}
                                                                onChange={handleInput('contestCommission')}
                                                                maxLength={5}
                                                                className="form-control" placeholder="Contest Commission"
                                                                // onInput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');"
                                                                id="contestCommission"
                                                                defaultValue={12} />
                                                        </div>
                                                    </div>
                                                    <div className="form-group">
                                                        <div className="input text">
                                                            <label htmlFor="min-withdraw-amount">Minimum Withdraw Amount (INR)</label>
                                                            <input
                                                                type="text"
                                                                name="minimumWithdraw"
                                                                value={settingForm.minimumWithdraw}
                                                                onChange={handleInput('minimumWithdraw')}
                                                                maxLength={7}
                                                                className="form-control"
                                                                placeholder="Minimum Withdraw Amount"
                                                                // onInput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');"
                                                                id="minimumWithdraw"
                                                                defaultValue={1} />
                                                        </div>
                                                    </div>
                                                    <div className="form-group">
                                                        <div className="input text">
                                                            <label htmlFor="team-limit">Team join Limit</label>
                                                            <input
                                                                type="text"
                                                                name="joinLimit"
                                                                value={settingForm.joinLimit}
                                                                onChange={handleInput('joinLimit')}
                                                                maxLength={5}
                                                                className="form-control"
                                                                placeholder="Team Limit"
                                                                // onInput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');"
                                                                id="joinLimit"
                                                                defaultValue={16} />
                                                        </div>
                                                    </div>
                                                    <div className="form-group">
                                                        <div className="input text">
                                                            <label htmlFor="apk-version">
                                                                APK Version
                                                            </label>
                                                            <input
                                                                type="text"
                                                                name="version"
                                                                value={settingForm.version}
                                                                onChange={handleInput('version')}
                                                                className="form-control"
                                                                placeholder="APK Version"
                                                                // onInput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');"
                                                                maxLength={255}
                                                                id="version"
                                                                defaultValue={5} />
                                                        </div>
                                                    </div>
                                                    <div className="form-group">
                                                        <label htmlFor="image">
                                                            APK File
                                                            <span className="required">
                                                                *
                                                            </span>
                                                        </label>
                                                        <div className="input file">
                                                            <input
                                                                type="file"
                                                                name="apk"
                                                                className="form-control"
                                                                id="apk" />
                                                        </div>
                                                    </div>
                                                    <div className="form-group">
                                                        <div className="input text">
                                                            <label htmlFor="tds">
                                                                TDS (On minimum 10000 INR)
                                                            </label>
                                                            <input
                                                                type="text"
                                                                name="tds"
                                                                value={settingForm.tds}
                                                                onChange={handleInput('tds')}
                                                                maxLength={5}
                                                                className="form-control"
                                                                placeholder="TDS"
                                                                // onInput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');"
                                                                id="tds"
                                                                defaultValue="31.2" />
                                                        </div>
                                                    </div>
                                                    <div className="form-group">
                                                        <div className="row">
                                                            <div className="col-md-4">
                                                                <label htmlFor="admin-background-color">
                                                                    Admin Background Color
                                                                </label>
                                                                <div className="input color">
                                                                    <input
                                                                        type="color"
                                                                        name="admin_background_color"
                                                                        id="admin-background-color"
                                                                        defaultValue="#2959cb" />
                                                                </div>
                                                            </div>
                                                            <div className="col-md-4">
                                                                <label htmlFor="app-color">App Color</label>
                                                                <div className="input color">

                                                                    <input type="color" name="app_color" id="app-color" defaultValue="#000053" />
                                                                </div>
                                                            </div>
                                                            <div className="col-md-4">
                                                                <label htmlFor="text-color">Text Color</label>
                                                                <div className="input color">
                                                                    <input type="color" name="text_color" id="text-color" defaultValue="#c0c0c0" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="card-footer">
                                                        <button type="submit" className="btn btn-primary submit">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div className='my-4'>Hidden from footer</div>
        </div>


    )

}
export default Settings