import React, { useState } from 'react'
import { Link } from "react-router-dom"
import Axios from 'axios'
const Profile = () => {
    const url = "http://65.0.213.3/pokerrz/WebServices/profile"
    // const [updateProfileForm, setUpdateProfileForm] = useState({
    //     firstname: "",
    //     lastname: "",
    //     teamName: "",
    //     email: "",
    //     phoneNumber: "",
    //     dateOfBirth: "",
    //     gender: "",
    //     country: "",
    //     state: "",
    //     address: "",
    //     city: "",
    //     pinCode: "",
    // });

    // let [records, setRecords] = useState([]);


    // const handleInput = (e) => {
    //     const name = e.target.name;
    //     const value = e.target.value
    //     // console.log(name, value)

    //     setUpdateProfileForm({ ...updateProfileForm, [name]: value })
    // }
    // const handleSubmit = (e) => {
    //     e.preventDefault();

    //     // Axios.post(url, {
    //     //     firstname: updateProfileForm.firstname,
    //     //     lastname: updateProfileForm.lastname,
    //     //     teamName: updateProfileForm.teamName,
    //     //     email: updateProfileForm.email,
    //     //     phoneNumber: updateProfileForm.phoneNumber,
    //     //     dateOfBirth: updateProfileForm.dateOfBirth,
    //     //     gender: updateProfileForm.gender,
    //     //     country: updateProfileForm.country,
    //     //     state: updateProfileForm.state,
    //     //     address: updateProfileForm.address,
    //     //     city: updateProfileForm.city,
    //     //     pinCode: updateProfileForm.pinCode,
    //     // })
    //     // const newRecord = { ...updateProfileForm, id: new Date().getTime().toString() }
    //     // // console.log(records)
    //     // setRecords([...records, newRecord]);
    //     // console.log("Profile", records)

    const [updateProfileForm, setValues] = useState({
        firstname: "",
        lastname: "",
        teamName: "",
        email: "",
        phoneNumber: "",
        dateOfBirth: "",
        gender: "",
        country: "",
        state: "",
        address: "",
        city: "",
        pinCode: "",
    });
    // const [loading, setLoading] = useState(false);

    const handleSubmit = async (e) => {
        console.log(updateProfileForm)
        e.preventDefault();

        // const { username, name, email, password } = updateProfileForm;
        const user = {
            firstname: updateProfileForm.firstname,
            lastname: updateProfileForm.lastname,
            teamName: updateProfileForm.teamName,
            email: updateProfileForm.email,
            phoneNumber: updateProfileForm.phoneNumber,
            dateOfBirth: updateProfileForm.dateOfBirth,
            gender: updateProfileForm.gender,
            country: updateProfileForm.country,
            state: updateProfileForm.state,
            address: updateProfileForm.address,
            city: updateProfileForm.city,
            pinCode: updateProfileForm.pinCode,
        };
        Axios.post(url, user)
        // await axios.post('${API)/signup', user);        
    };

    const handleInput = name => e => {
        setValues({ ...updateProfileForm, [name]: e.target.value });
    };


    return (
        <div>
            <div className="content-wrapper">
                <section className="content-header">
                    <div className="container-fluid">
                        <div className="row mb-2">
                            <div className="col-sm-6">
                                <h1>Update Profile</h1>
                            </div>
                            <div className="col-sm-6">
                                <ol className="breadcrumb float-sm-right">
                                    <li className="breadcrumb-item">
                                        <Link to="/">Home</Link>
                                    </li>
                                    <li className="breadcrumb-item active">Update Profile</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="content">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="card card-primary">
                                    <form onSubmit={handleSubmit}>
                                        <div style={{ display: 'none' }}>
                                            <input
                                                type="hidden"
                                                name="_method"
                                                defaultValue="PUT" />
                                        </div>
                                        <div className="card-body">
                                            <div className="row">
                                                <div className="col-xs-6 col-sm-6 col-md-6">
                                                    <div className="form-group">
                                                        <div className="input text">
                                                            <label htmlFor="first-name">
                                                                First Name
                                                                <span className="required text-danger">*</span>
                                                            </label>
                                                            <input
                                                                type="text"
                                                                name="firstname"
                                                                value={updateProfileForm.firstname}
                                                                onChange={handleInput('firstname')}
                                                                maxLength={40}
                                                                className="form-control"
                                                                placeholder="First Name"
                                                                id="first-name"
                                                                defaultValue="kalra" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-xs-6 col-sm-6 col-md-6">
                                                    <div className="form-group">
                                                        <div className="input text">
                                                            <label htmlFor="last-name">Last Name
                                                                <span className="required text-danger">*</span></label>
                                                            <input type="text"
                                                                name="lastname"
                                                                value={updateProfileForm.lastname}
                                                                onChange={handleInput('lastname')}
                                                                maxLength={40}
                                                                className="form-control"
                                                                placeholder="Last Name "
                                                                id="last-name"
                                                                defaultValue />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-xs-6 col-sm-6 col-md-6">
                                                    <div className="form-group">
                                                        <div className="input text">
                                                            <label htmlFor="team-name">
                                                                Team Name
                                                                <span className="required text-danger">*</span>
                                                            </label>
                                                            <input type="text"
                                                                name="teamName"
                                                                value={updateProfileForm.teamName}
                                                                onChange={handleInput('teamName')}
                                                                className="form-control"
                                                                placeholder="Team Name"
                                                                maxLength={255}
                                                                id="team-name"
                                                                defaultValue="test" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-xs-6 col-sm-6 col-md-6">
                                                    <div className="form-group">
                                                        <div className="input email">
                                                            <label htmlFor="email">
                                                                Email
                                                                <span className="required text-danger">*</span>
                                                            </label>
                                                            <input type="email"
                                                                name="email"
                                                                value={updateProfileForm.email}
                                                                onChange={handleInput('email')}
                                                                className="form-control"
                                                                placeholder="E-Mail Addeess"
                                                                readOnly="readonly"
                                                                maxLength={255} id="email"
                                                                defaultValue="admin@gmail.com" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-xs-6 col-sm-6 col-md-6">
                                                    <div className="form-group">
                                                        <div className="input tel">
                                                            <label htmlFor="phone">
                                                                Phone Number
                                                                <span className="required text-danger">
                                                                    *
                                                                </span>
                                                            </label>
                                                            <input type="tel"
                                                                name="phoneNumber"
                                                                value={updateProfileForm.phoneNumber}
                                                                onChange={handleInput('phoneNumber')}
                                                                className="form-control"
                                                                placeholder="Phone Number"
                                                                maxLength={10}
                                                                id="phone"
                                                                defaultValue={1234567891} />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-xs-6 col-sm-6 col-md-6">
                                                    <div className="form-group">
                                                        <div className="input text">
                                                            <label htmlFor="date-of-bith">Date of Birth</label>
                                                            <input
                                                                type="text"
                                                                name="dateOfBirth"
                                                                value={updateProfileForm.dateOfBirth}
                                                                onChange={handleInput}
                                                                className="form-control DOB"
                                                                placeholder="Date of Birth"
                                                                readOnly="readonly"
                                                                maxLength={50}
                                                                id="date-of-bith" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-xs-6 col-sm-6 col-md-6">
                                                    <div className="form-group">
                                                        <div className="input select">
                                                            <label htmlFor="gender">Gender</label>
                                                            <select
                                                                name="gender"
                                                                value={updateProfileForm.gender}
                                                                onChange={handleInput('gender')}
                                                                className="form-control"
                                                                id="gender">
                                                                <option value>Select Gender</option>
                                                                <option >
                                                                    Male
                                                                </option>
                                                                <option
                                                                    selected="selected">Female</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-xs-6 col-sm-6 col-md-6">
                                                    <div className="form-group">
                                                        <div className="input text">
                                                            <label htmlFor="country">Country</label>
                                                            <input type="text"
                                                                name="country"
                                                                value={updateProfileForm.country}
                                                                onChange={handleInput('country')}
                                                                className="form-control"
                                                                placeholder="Country"
                                                                maxLength={255}
                                                                id="country" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-xs-6 col-sm-6 col-md-6">
                                                    <div className="form-group">
                                                        <div className="input text">
                                                            <label htmlFor="state">State</label>
                                                            <input type="text"
                                                                name="state"
                                                                value={updateProfileForm.state}
                                                                onChange={handleInput('state')}
                                                                className="form-control"
                                                                placeholder="State"
                                                                maxLength={255}
                                                                id="state" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-xs-6 col-sm-6 col-md-6">
                                                    <div className="form-group">
                                                        <div className="input text">
                                                            <label htmlFor="address">Address</label>
                                                            <input
                                                                type="text"
                                                                name="address"
                                                                value={updateProfileForm.address}
                                                                onChange={handleInput('address')}
                                                                className="form-control"
                                                                placeholder="Address"
                                                                id="address" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-xs-6 col-sm-6 col-md-6">
                                                    <div className="form-group">
                                                        <div className="input text">
                                                            <label htmlFor="city">City</label>
                                                            <input
                                                                type="text"
                                                                name="city"
                                                                value={updateProfileForm.city}
                                                                onChange={handleInput('city')}
                                                                className="form-control"
                                                                placeholder="City"
                                                                maxLength={255}
                                                                id="city" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-xs-6 col-sm-6 col-md-6">
                                                    <div className="form-group">
                                                        <div className="input text">
                                                            <label htmlFor="postal-code">Pin Code</label>
                                                            <input
                                                                type="text"
                                                                name="pinCode"
                                                                value={updateProfileForm.pinCode}
                                                                onChange={handleInput('pinCode')}
                                                                className="form-control"
                                                                placeholder="Pin Code"
                                                                maxLength={50}
                                                                id="postal-code" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="card-footer">
                                            <button type="submit" className="btn btn-primary">update</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div className="my-4">blank Footer</div>
        </div>


    )
}


export default Profile