import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import Axios from 'axios'
const ChangePassword = () => {

    const url = "http://65.0.213.3/pokerrz/WebServices/profile"
    const [changePassword, setchangePassword] = useState({
        oldPass: "",
        newPass: "",
        confirmPass: "",

    });

    const handleSubmit = async (e) => {
        console.log(changePassword)
        e.preventDefault();
        const pass = {
            oldPass: changePassword.oldPass,
            newPass: changePassword.newPass,
            confirmPass: changePassword.confirmPass,
        };
        Axios.post(url, pass)
    }
    const handleInput = name => e => {
        setchangePassword({ ...changePassword, [name]: e.target.value });
    };


    return (
        <div>
            <div className="content-wrapper">
                <section className="content-header">
                    <div className="container-fluid">
                        <div className="row mb-2">
                            <div className="col-sm-6">
                                <h1>Change Password</h1>
                            </div>
                            <div className="col-sm-6">
                                <ol className="breadcrumb float-sm-right">
                                    <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                                    <li className="breadcrumb-item active">Change Password</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="content">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="card card-primary">
                                    <form onSubmit={handleSubmit}
                                        method="post"
                                        action="/admin/users/change-password">
                                        <div style={{ display: 'none' }}>
                                            <input
                                                type="hidden"
                                                name="_method"
                                                defaultValue="PUT" />
                                        </div>
                                        <div className="card-body">
                                            <div className="row">
                                                <div className="col-xs-6 col-sm-6 col-md-6">
                                                    <div className="form-group">
                                                        <div className="input password">
                                                            <label htmlFor="old-password">
                                                                Old Password
                                                                <span className="text-danger">
                                                                    *
                                                                </span>
                                                            </label>
                                                            <input
                                                                type="password"
                                                                name="oldPass"
                                                                value={changePassword.oldPass}
                                                                onChange={handleInput('oldPass')}
                                                                className="form-control"
                                                                placeholder="Old Password"
                                                                id="old-password" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-xs-6 col-sm-6 col-md-6">
                                                    <div className="form-group">
                                                        <div className="input password">
                                                            <label htmlFor="password">New Password <span className="text-danger">
                                                                *
                                                            </span>
                                                            </label>
                                                            <input
                                                                type="password"
                                                                name="newPass"
                                                                value={changePassword.newPass}
                                                                onChange={handleInput('newPass')}
                                                                className="form-control"
                                                                placeholder="New Password"
                                                                id="password"
                                                                defaultValue />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-xs-6 col-sm-6 col-md-6">
                                                    <div className="form-group">
                                                        <div className="input password">
                                                            <label htmlFor="confirm-password">
                                                                Confirm Password
                                                                <span className="text-danger">
                                                                    *
                                                                </span>
                                                            </label>
                                                            <input
                                                                type="password"
                                                                name="confirmPass"
                                                                value={changePassword.confirmPass}
                                                                onChange={handleInput('confirmPass')}
                                                                className="form-control"
                                                                placeholder="Confirm Password"
                                                                id="confirm-password" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="card-footer">
                                            <button type="submit" className="btn btn-primary submit">Submit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

        </div>
    )

}
export default ChangePassword 
