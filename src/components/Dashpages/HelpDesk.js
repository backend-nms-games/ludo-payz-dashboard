// import logo from "../Helpdesk/assets/chat-icon.png"
// // import "../../style/Helpdesk.css"
// import { useState, useEffect, useRef } from "react";
// import CreateUser from "../Helpdesk/CreateUser";
// import OnlineUsers from "../Helpdesk/OnlineUsers";
// import MessagesControl from "../Helpdesk/MessagesControl";
// import { io } from "socket.io-client";
// const socket = io(`http://localhost:5000`);


// function HelpDesk() {

//     const [step, setStep] = useState(0);
//     const [username, setUsername] = useState("");
//     const [receiver, setReceiver] = useState("");
//     const [avatar, setAvatar] = useState("");
//     const [media, setMedia] = useState(null);
//     const [users, setUsers] = useState({});
//     const [message, setMessage] = useState("");
//     const [groupMessage, setGroupMessage] = useState({});
//     const receiverRef = useRef(null);

//     const sortNames = (username1, username2) => {
//         return [username1, username2].sort().join("-");
//     }

//     const onCreateUser = () => {
//         console.log(username)

//         socket.emit("new_user", username);
//         const a = Math.floor(Math.random() * 8) + ".jpg";
//         setAvatar(a);
//         setStep(prevStep => prevStep + 1);
//     };

//     const onUserSelect = (username) => {
//         setReceiver(username);
//         receiverRef.current = username;
//         setStep((prevStep) => prevStep + 1);
//     }
//     const sendMessage = (e) => {
//         e.preventDefault();

//         const data = {
//             sender: username,
//             receiver,
//             message,
//             media,
//             avatar
//         };

//         socket.emit("send_message", data);


//         const key = sortNames(username, receiver);
//         const tempGroupMessage = { ...groupMessage }
//         if (key in tempGroupMessage) {
//             tempGroupMessage[key] = [...tempGroupMessage[key], data]
//         } else {
//             tempGroupMessage[key] = [data];
//         }

//         setGroupMessage({ ...tempGroupMessage });

//         // console.log({ groupMessage })

//         // console.log({ message });
//     };

//     useEffect(() => {
//         socket.on("all_users", (users) => {
//             console.log({ users });
//             setUsers(users);
//         });

//         socket.on("new_message", (data) => {
//             console.log(data)
//             setGroupMessage((prevGroupMessage) => {
//                 const messages = { ...prevGroupMessage };
//                 const key = sortNames(data.sender, data.receiver);
//                 if (key in messages) {
//                     messages[key] = [...messages[key], data];
//                 } else {
//                     messages[key] = [data];
//                 }
//                 return { ...messages };
//             });
//         });
//     }, []);
//     console.log(groupMessage)

//     return (
//         <div className="content-wrapper">
//             <div className="body">
//                 <div className="App">
//                     <header className="app-header">
//                         <img src={logo} alt="" />
//                         <div className="app-name b-500 primaryColor">Help Desk</div>
//                     </header>
//                     <div className="chat-system">
//                         <div className="chat-box">
//                             {/* step 1: ask username ro email */}

//                             {step === 0 ? (
//                                 <CreateUser onCreateUser={onCreateUser}
//                                     value={username}
//                                     onChange={(e) => setUsername(e.target.value)} />
//                             ) : null}
//                             {/* step:2 show all available users */}
//                             {step === 1 ? (
//                                 <OnlineUsers onUserSelect={onUserSelect}
//                                     users={users}
//                                     username={username}
//                                 />
//                             ) : null}
//                             {/* step 3: select user and switch to chat window */}
//                             {step === 2 ? (
//                                 <MessagesControl
//                                     value={message}
//                                     onChange={(e) => setMessage(e.target.value)}
//                                     sendMessage={sendMessage}
//                                     groupMessage={groupMessage}
//                                     sortNames={sortNames}
//                                     username={username}
//                                     receiver={receiver}
//                                 />
//                             ) : null}
//                         </div>
//                     </div>
//                 </div>
//             </div>
//         </div>
//     )
// }
// export default HelpDesk