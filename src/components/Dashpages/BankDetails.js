import React, { useState, useEffect } from 'react' 
import DataTable from 'react-data-table-component';
import { apiBaseURL } from '../../config';
import axios from "axios"
import moment from 'moment';
const BankDetails = () => {

    const [banksData, setbanksData] = useState([]);
    
    const getUsers = async () => {
        await axios.get(`${apiBaseURL}/money/usersAccoutDetails`).then(function (response) { 
            if(response.data.status ===200){
                // console.log(response.data.data)
                setbanksData(response.data.data)
            } 
          })
          .catch(function (error) {
            // handle error 
            console.log(error);
          }); 
    }
    useEffect(() => {
        getUsers();
        // console.log(banksData)
    }, []);
    const approveStatus = async (id) => {
      // e.preventDefault();

      //  
      alert(id)
  };
    const columns = [
        {
          name: "#Serial No",
          selector: (row, index) => index+1,
          sortable: true
        },
        {
            name: "Phone Number",
            selector: "phone",
            string:true,
            sortable: true
          },
          {
            name: "Card Holder Name",
            selector: "account_holder_name",
            string:true,
            sortable: true
          },
        {
          name: "Account Number",
          selector: "account_number",
          string:true,
          sortable: true
        },
           
          {
            name: "IFSC Code",
            selector: "ifsc_code",
            string:true,
            sortable: true
          },
          {
            name: "Upi ID",
            selector: "upi_id",
            string:true,
            sortable: true
          },
          
          {
            name: "Status",
            selector: (row) => <p>{(row.is_verified==1)?'Verified':'Pending'}</p>
             , 
          },
        {
          name: "Actions",
          cell: ( row ) => (
            <button
              aria-label="delete"
              color="secondary"
              onClick={() => approveStatus(row.transaction_id)}
            >  
              Approve 
            </button>
          )},
          {
            cell: ( row ) => (
              <button
                aria-label="delete"
                color="secondary"
  
                onClick={() => approveStatus(row.transaction_id)}
              >  
                Reject 
              </button>
            )
          }
            
          ];
          
    return (
        <div>
            <div className="content-wrapper">
                <section className="content admin_users">
                    <div className="r o w">
                        <div className="col-md-12">
                            <div className="card">
                                <div className="card-header">
                                    <h3 className="card-title" style={{ display: 'inline-block' }}></h3>
                                </div>
                                <div className="card-body">
                                  
                                <DataTable
                                    title="Bank Details"
                                    columns={columns}
                                    data={banksData}
                                    highlightOnHover
                                    pagination
                                    paginationPerPage={6}
                                    paginationRowsPerPageOptions={[6, 15, 25, 50]}
                                    paginationComponentOptions={{
                                    rowsPerPageText: 'Records per page:',
                                    rangeSeparatorText: 'out of',
                                    }}
                                />
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div >

        </div >
    )
}
export default BankDetails
