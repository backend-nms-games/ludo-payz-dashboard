import React, { useState } from 'react'
import { Button } from '@mui/material'
import Stack from '@mui/material/Stack';
import TextField from '@mui/material/TextField';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DesktopDatePicker from '@mui/lab/DesktopDatePicker';
import Axios from 'axios'
const UsersList = () => {
    const url = "http://65.0.213.3/pokerrz/WebServices/profile"

    const [subAdminAdd, setUserRegistraion] = useState({
        firstname: "",
        lastname: "",
        email: "",
        phoneNumber: "",
        password: "",
        dateOfBirth: "",
        gender: ""

    });

    // const [records, setRecords] = useState([]);


    const handleSubmit = async (e) => {
        console.log(subAdminAdd)
        e.preventDefault();

        const user = {
            firstname: subAdminAdd.firstname,
            lastname: subAdminAdd.lastname,
            email: subAdminAdd.email,
            phoneNumber: subAdminAdd.phoneNumber,
            dateOfBirth: subAdminAdd.dateOfBirth,
            gender: subAdminAdd.gender,
            password: subAdminAdd.password

        };
        Axios.post(url, user)

    }
    const handleInput = name => e => {
        setUserRegistraion({ ...subAdminAdd, [name]: e.target.value });
    };

    return (
        <div>
            <div className="content-wrapper">
                <section className="content-header">
                    <div className="container-fluid">
                        <div className="row mb-2">
                            <div className="col-sm-6">
                                <h1>Add Sub Admin</h1>
                            </div>
                            <div className="col-sm-6">
                                <ol className="breadcrumb float-sm-right">
                                    <li className="breadcrumb-item"><a href="/">Home</a></li>
                                    <li className="breadcrumb-item active">Add Sub Admin</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="content admins_add">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="card card-primary">
                                    <div className="card-header">
                                        <h3 className="card-title">Fill Form</h3>
                                    </div>
                                    <form
                                        onSubmit={handleSubmit}>
                                        <div
                                            style={{ display: 'none' }}>
                                            <input
                                                type="hidden"
                                                name="_method"
                                                defaultValue="POST" />
                                        </div>
                                        <div className="card-body">
                                            <div className="row">
                                                <div className="col-sm-6 col-md-6">
                                                    <div className="form-group">
                                                        <div className="input text">
                                                            <label htmlFor="first-name">
                                                                First Name
                                                                <span className="text-danger">*</span>
                                                            </label>
                                                            <input
                                                                type="text"
                                                                name="firstname"
                                                                value={subAdminAdd.firstname}
                                                                onChange={handleInput('firstname')}
                                                                maxLength={30}
                                                                className="form-control"
                                                                max={20} />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-sm-6 col-md-6">
                                                    <div className="form-group">
                                                        <div className="input text">
                                                            <label htmlFor="last-name">
                                                                Last Name
                                                                <span className="text-danger">*</span>
                                                            </label>
                                                            <input
                                                                type="text"
                                                                name="lastname"
                                                                value={subAdminAdd.lastname}
                                                                onChange={handleInput('lastname')}
                                                                maxLength={30}
                                                                className="form-control"

                                                                max={20}
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-sm-6 col-md-6">
                                                    <div className="form-group">
                                                        <div className="input email">
                                                            <label htmlFor="email">
                                                                Email
                                                                <span className="text-danger">
                                                                    *
                                                                </span>
                                                            </label>
                                                            <input
                                                                type="email"
                                                                name="email"
                                                                value={subAdminAdd.email}
                                                                onChange={handleInput('email')}
                                                                maxLength={50}
                                                                className="form-control"

                                                                id="email" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-sm-6 col-md-6">
                                                    <div className="form-group">
                                                        <div className="input tel">
                                                            <label htmlFor="phone">
                                                                Phone Number
                                                                <span className="text-danger">
                                                                    *
                                                                </span>
                                                            </label>
                                                            <input

                                                                type="number"
                                                                name="phoneNumber"
                                                                value={subAdminAdd.phoneNumber}
                                                                onChange={handleInput('phoneNumber')}
                                                                maxLength={15}
                                                                className="form-control"
                                                                placeholder="Phone Number"

                                                                id="phoneNumber" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-sm-6 col-md-6">
                                                    <div className="form-group">
                                                        <div className="input password">
                                                            <label htmlFor="password">
                                                                Password
                                                                <span className="text-danger">
                                                                    *
                                                                </span>
                                                            </label>
                                                            <input
                                                                type="password"
                                                                name="password"
                                                                value={subAdminAdd.password}
                                                                onChange={handleInput('password')}
                                                                className="form-control"
                                                                id="password"
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-sm-6 col-md-6">
                                                    <div className="form-group">
                                                        <div className="input text">
                                                            <label
                                                                htmlFor="date-of-bith">
                                                                Date of Birth
                                                            </label>
                                                            <LocalizationProvider dateAdapter={AdapterDateFns}>
                                                                <Stack spacing={3}>
                                                                    <DesktopDatePicker
                                                                        className="form-control DOB"
                                                                        label="Date of Birth"
                                                                        inputFormat="MM/dd/yyyy"
                                                                        name="dateOfBirth"
                                                                        value={subAdminAdd.dateOfBirth}
                                                                        onChange={handleInput('dateOfBirth')}
                                                                        renderInput={(params) => <TextField {...params} />}
                                                                    />
                                                                </Stack>
                                                            </LocalizationProvider>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-sm-6 col-md-6">
                                                    <div className="form-group">
                                                        <div className="input select">
                                                            <label htmlFor="gender">
                                                                Gender
                                                            </label>
                                                            <select
                                                                name="gender"
                                                                value={subAdminAdd.gender}
                                                                onChange={handleInput('gender')}
                                                                className="form-control"
                                                                id="gender">
                                                                <option value>
                                                                    Select Gender
                                                                </option>
                                                                <option selected="selected">
                                                                    Male
                                                                </option>
                                                                <option >
                                                                    Female
                                                                </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-sm-6 col-md-6">
                                                    <div className="form-group">
                                                        <label htmlFor="profile-picture">
                                                            Profile Picture
                                                        </label>
                                                        <div className="input file">
                                                            <input
                                                                type="file"
                                                                name="image"

                                                                // onChange="loadGroupFile(event)"
                                                                accept="image/*"
                                                                className id="image" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-sm-6 col-md-6">
                                                    <div className="form-group">
                                                        <label htmlFor="module-access">Module Access</label>
                                                        <select
                                                            name="modules"
                                                            value=""
                                                            style={{ width: '100%', fontFamily: 'inherit' }}
                                                            id="module_access"
                                                            multiple="multiple">
                                                            <option value="users">Users</option>
                                                            <option value="games">Games Manager</option>
                                                            <option value="category">Category Manager</option>
                                                            <option value="contest">Contest Manager</option>
                                                            <option value="series_contest">Series Contest</option>
                                                            <option value="schedule_contest">Schedule Contest</option>
                                                            <option value="point_system">Point System</option>
                                                            <option value="tds">TDS</option>
                                                            <option value="players">Player Manager</option>
                                                            <option value="payment_offer">Payment Offer</option>
                                                            <option value="banner">Banner Manager</option>
                                                            <option value="teams">Teams Manager</option>
                                                            <option value="withdraw_requests">
                                                                Withdraw Request
                                                            </option>
                                                            <option value="notification">Notification Manager</option>
                                                            <option value="blog">Blog</option>
                                                            <option value="content">Contents Manager</option>
                                                            <option value="setting">Settings</option>
                                                            <option value="email">Email Template</option>
                                                            <option value="profile">Profile</option>
                                                            <option value="change_password">Change Password</option>
                                                            <option value="magic_box">Magic Box</option>
                                                            <option value="home_banners">Home Banners</option>
                                                            <option value="bots">Bots</option>
                                                            <option value="advertisements">Advertisement</option>
                                                        </select>
                                                        <input
                                                            type="hidden"
                                                            name="module_access"
                                                            className="module_accesss" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="card-footer">
                                            <Button
                                                type="submit"
                                                variant="contained"
                                                className="btn btn-primary submit">
                                                Submit
                                            </Button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <div className='my-4'>hidden</div>
            </div>
        </div>

    )

}
export default UsersList