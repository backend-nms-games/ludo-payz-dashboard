import React, { useState, useEffect } from 'react' 
import DataTable from 'react-data-table-component';
import { apiBaseURL } from '../../config';
import axios from "axios"

const WithdrawRequest = () => {

    const [trasactions, setUsers] = useState([]);
    
    const getUsers = async () => {
        await axios.get(`${apiBaseURL}/money/withdrawRequestHistory`).then(function (response) { 
            if(response.data.status ===200){
                console.log(response.data.data)
                setUsers(response.data.data)
            } 
          })
          .catch(function (error) {
            // handle error 
            console.log(error);
          }); 
    }
    useEffect(() => {
        getUsers();
    }, []);
    const approveTransaction = async (id) => {
      // e.preventDefault();

      //  
      alert(id)
  };
    const columns = [
        {
          name: "#ID",
          selector: (row, index) => index+1,
          sortable: true
        },
        {
            name: "Phone Number",
            selector: "phone",
            string:true,
            sortable: true
          },
        {
            name: "Username",
            selector: "username",
            string:true,
            sortable: true
          },
          {
            name: "Email ID",
            string:true,
            selector: "email",
            sortable: true
          },
          {
            name: "Transactions ID",
            string:true,
            selector: (row) => <p>{(row.tournament_id==null)?row.local_txn_id:row.tournament_id}</p>
             ,
            sortable: true
          },
          
          {
            name: "Status", 
            sortable: true, 
            selector: (row) => <a>{ (row.is_type==7)?"Withdraw Request":'Pending'}</a>
             
          },
        {
          name: "Actions",
          // selector: "amount",
          sortable: true, 
          button:true,
          cell: ( row ) => (
            <button
              aria-label="delete"
              color="secondary"
              onClick={() => approveTransaction(row.transaction_id)}
            >  
              Approve 
            </button>
          )
        }
          ];
         
    return (
        <div>
            <div className="content-wrapper">
                <section className="content admin_users">
                    <div className="r o w">
                        <div className="col-md-12">
                            <div className="card">
                                <div className="card-header">
                                    <h3 className="card-title" style={{ display: 'inline-block' }}></h3>
                                </div>
                                <div className="card-body">
                                <DataTable
                                    title="Transactions History"
                                    columns={columns}
                                    data={trasactions}
                                    highlightOnHover
                                    pagination
                                    paginationPerPage={6}
                                    paginationRowsPerPageOptions={[6, 15, 25, 50]}
                                    paginationComponentOptions={{
                                    rowsPerPageText: 'Records per page:',
                                    rangeSeparatorText: 'out of',
                                    }}
                                />
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div >

        </div >
    )
}
export default WithdrawRequest
