import React from 'react'
import { Link } from "react-router-dom"



const SideMenu = () => {

    return (
        <div>
            <aside className="main-sidebar sidebar-dark-primary elevation-4">

                <div className="sidebar">

                    <div className="user-panel mt-3 pb-3 mb-3 d-flex">
                    </div>
                    <div className="form-inline">
                        <div className="input-group" data-widget="sidebar-search">
                            <input
                                className="form-control form-control-sidebar"
                                type="search"
                                placeholder="Search"
                                aria-label="Search" />
                            <div className="input-group-append">
                                <button className="btn btn-sidebar">
                                    <i className="fas fa-search fa-fw" />
                                </button>
                            </div>
                        </div>
                    </div>
                    {/* Sidebar Menu */}
                    <nav className="mt-2">
                        <ul
                            className="nav nav-pills nav-sidebar flex-column"
                            data-widget="treeview"
                            role="menu"
                            data-accordion="false">
                            <li className="nav-item">
                                <Link to="/" className="nav-link">
                                    <i className="nav-icon fas fa-tachometer-alt mx-1"></i>
                                    <p> Dashboard
                                    </p>
                                </Link>
                            </li>

                           
                            <li className="nav-item">
                                <Link to="#" className="nav-link">
                                    <i className="nav-icon fas fa-users" />

                                    <p>
                                        Users
                                        <i className="fas fa-angle-left right" />
                                    </p>
                                </Link>
                                <ul className="nav nav-treeview">
                                    <li className="nav-item">
                                        <Link to="/userlist" className="nav-link">
                                            <i className="far fa-circle nav-icon" />
                                            <p>List</p>
                                        </Link>
                                    </li>
                                    <li className="nav-item">
                                        <Link to="/useradd" className="nav-link">
                                            <i className="far fa-circle nav-icon" />
                                            <p>Add</p>
                                        </Link>
                                    </li>
                                </ul>
                            </li>
                            <li className="nav-item">
                                <Link to="#" className="nav-link">
                                    <i className="nav-icon fas fa-rupee-sign"></i>
                                    <p>Transactions
                                        <i className="fas fa-angle-left right" />
                                    </p>
                                </Link>
                                <ul className="nav nav-treeview">
                                    <li className="nav-item">
                                        <Link to="/transactionhistory" className="nav-link">
                                            <i className="nav-icon fas fa-wallet"></i>
                                            <p>Transaction History</p>
                                        </Link>
                                    </li>
                                    <li className="nav-item">
                                        <Link to="/withdraw" className="nav-link">
                                            <i className="nav-icon fas fa-cash-register"></i>
                                            <p>Withdraw Request</p>
                                        </Link>
                                    </li>
                                </ul>
                            </li>
                            <li className="nav-item">
                                <Link to="#" className="nav-link">
                                <i className="nav-icon far fa-id-card"></i>
                                    <p>Kyc Details
                                        <i className="fas fa-angle-left right" />
                                    </p>
                                </Link>
                                <ul className="nav nav-treeview">
                                    <li className="nav-item">
                                        <Link to="/bankdetails" className="nav-link">
                                            <i className="nav-icon fas fa-wallet"></i>
                                            <p>Bank Details</p>
                                        </Link>
                                    </li>
                                    <li className="nav-item">
                                        <Link to="/UserKyc" className="nav-link">
                                        <i className="nav-icon far fa-id-card"></i>
                                            <p>Kyc</p>
                                        </Link>
                                    </li>
                                </ul>
                            </li>
                            
                            <li className="nav-item">
                                <Link to="/helpdesk" className="nav-link">
                                    <i className="nav-icon fas fa-ticket-alt"></i>
                                    <p>Help Desk</p>
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link to="/settings" className="nav-link">
                                    <i className="nav-icon fas fa-users-cog"></i>
                                    <p>Settings</p>
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link to="/profile" className="nav-link">
                                    <i className="nav-icon fas fa-user-alt"></i>
                                    <p>Profile</p>
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link to="/password" className="nav-link">
                                    <i className="nav-icon fas fa-lock"></i>
                                    <p>Change Password</p>
                                </Link>
                            </li>
                        </ul>
                    </nav>
                </div>
            </aside>
        </div>
    )
}
export default SideMenu
