import React from "react";
import { Button, Grid, Paper, TextField, Typography, } from '@material-ui/core'
import { Link } from "react-router-dom";


const ForgetPassword = () => {

    return (
        <div >

            <Grid className="">
                <Paper elevation={15} className="paperStylesignup"  >
                    <Grid align='center'>
                        <h4>Enter your details</h4>
                    </Grid>
                    <div>
                        <TextField className="my-2 d-flex justify-content-center" id="standard-basic" label="Enter your email" type='email' variant="standard" />
                    </div>
                    <div >
                        <TextField className="my-2 d-flex justify-content-centered" id="standard-basic" label="Enter your OTP" type="number" min="1" max="5" variant="standard" />
                    </div>
                    <div >
                        <TextField className="my-2 d-flex justify-content-centered" id="standard-basic" label="Set password" type='password' variant="standard" />
                    </div>

                    <Button className="my-4 bg-primary" variant="contained" fullWidth>Login</Button>
                    <Typography className="my-2 ">
                        <p>Do you have account?
                            <Link className="text-decoration-none" to="/signup">
                                sign Up here
                            </Link>
                        </p>
                    </Typography>
                    <Typography className="my-1">
                        <p>Back to
                            <Link className="text-decoration-none" to="/login">
                                Login page
                            </Link>
                        </p>
                    </Typography>
                </Paper>
            </Grid>
        </div>
    )
}
export default ForgetPassword