import React, { useState, useEffect } from 'react'; 
import { Button, Grid, Paper, TextField } from '@material-ui/core'
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import logo from '../image/DvsT.jpg';
import { useHistory } from "react-router-dom"
import "../style/Login.css"
import axios from "axios"
import { apiBaseURL } from '../config';
const Login = () => {
    const [values, setValues] = useState({ 
        email: 'rajendra@nmsgames.com',
        password: 'Rajemdt',
    });
    const [loading, setLoading] = useState(false);
    const history = useHistory();
    const handleSubmit = async (e) => {
        e.preventDefault();

        const { email, password } = values;
        const user = {email, password}; 
        await axios.post(`${apiBaseURL}/auth/login`, user).then(function (response) { 
            if(response.data.status ===200){
                localStorage.setItem("user-info", JSON.stringify(response.data.user))
                history.push("/")
            } 
          })
          .catch(function (error) {
            // handle error
            history.push("/login")
            console.log(error);
          }); 
              
    };

    const handleChange = name => e => {
        console.log(e.target.value)
        setValues({ ...values, [name]: e.target.value });
    }; 
    useEffect(() => {
        console.log(localStorage.getItem("user-info"))
        if (localStorage.getItem("user-info")) {
            history.push("/")
        }else{
            history.push("/login")
        }
    }, []);
 

    return (
        <div >
            <div className="boxStyle d-flex justify-content-center">
                <img src={logo} width="170" height="150" alt='logo_dragon vs tiger' />
            </div>
            <form  onSubmit={handleSubmit}>
            <Grid>
                <Paper elevation={15} className="paperStyle">
                    <Grid align='center'>
                        <h4>Sign In to start your session</h4>
                    </Grid>
                    
                        <div className="">
                            <TextField
                                className="my-4 d-flex justify-content-center"
                                id="outlined-basic"
                                label="Enter your Email"
                                value={values.email}
                                onChange={handleChange('email')}
                                name='email'
                                type='email'
                                variant="standard" />
                        </div>
                        <div className="">
                            <TextField
                                className="my-2 d-flex justify-content-centered"
                                id="outlined-basic"
                                label="Enter your password"
                                name='password'
                                value={values.password}
                                onChange={handleChange('password')}
                                type='password'
                                variant="standard" />
                        </div>
                        <div className="my-2">
                            <FormGroup>
                                <FormControlLabel control={<Checkbox defaultChecked />} label="Remember me" />
                            </FormGroup>
                        </div>

                        <Button
                        type="submit"
                            className="my-2"
                            variant="contained"
                            color="primary" 
                            fullWidth

                        >Sign In
                        </Button>

                   
                </Paper>

            </Grid>
            </form>
        </div>
    )
}
export default Login